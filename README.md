# CheapShark for Android - video game deals browser

[![Kotlin Version](https://img.shields.io/badge/Kotlin-1.9.23-blue)](http://kotlinlang.org/)
[![Gradle](https://img.shields.io/badge/Gradle-8.5.0-blue)](https://lv.binarybabel.org/catalog/gradle/latest)
[![API](https://img.shields.io/badge/API-22%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=22)
[![License](https://img.shields.io/badge/License-Apache%202.0-lightgrey.svg)](http://www.apache.org/licenses/LICENSE-2.0)

## Overview

**CheapShark** is an Android front-end client for the [CheapShark API](https://apidocs.cheapshark.com/) that allows browsing and discovering the best deals for PC games.

The goal of this project is to showcase the usage of some of the popular frameworks in the field of Android development.

**CheapShark for Android** is a work-in-progress, see [roadmap](#roadmap) section below.

**CheapShark for Android** is an independent project and is not affiliated with [CheapShark](https://www.cheapshark.com/) .

### Screenshots

<!--suppress HtmlRequiredAltAttribute, CheckImageSize -->
<img src="/screenshots/cheapshark_demo.gif" width="200" />
 <img src="/screenshots/screenshot_01.png" width="200" />
 <img src="/screenshots/screenshot_02.png" width="200" /> 
 <img src="/screenshots/screenshot_03.png" width="200" />

## Installation requirements  

- Android Studio Koala | 2024.1.1 and above (Gradle/AGP compatibility).
- Android device/emulator with API 22 or higher.
- No API keys required.

## Architecture
- Single activity with [Navigation](https://developer.android.com/guide/navigation/) Component and [Safe Args](https://developer.android.com/guide/navigation/navigation-pass-data#Safe-args)
- [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) pattern that allows the separation of the UI (View) from the business logic (Model) by means of ViewModel.
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture) Collection of tools that help design robust, testable, and maintainable apps. Some of the tools include: 
[Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle), [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) and [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) Group of practices that enables building projects with the following characteristics: testable, UI-independent and frameworks/libraries independent. 

## Tech-stack
The project utilises some of the most common/popular libraries/tools in the Android ecosystem.
 
- [Jetpack](https://developer.android.com/jetpack)
  - [Android KTX](https://developer.android.com/kotlin/ktx.html) - Set of Kotlin extensions that provide concise, idiomatic Kotlin to Jetpack, Android platform, and other APIs.
  - [Data Binding](https://developer.android.com/topic/libraries/data-binding/) - Support library that allows binding UI components within layouts to data sources using a declarative format.
  - [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle) - Package that allow writing components that are Android-lifecycle-aware.
  - [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Lifecycle-aware observable data container class.
  - [Navigation](https://developer.android.com/guide/navigation/) - Component that helps implementing user navigation across different screens/ui components.
  - [Paging v3](https://developer.android.com/topic/libraries/architecture/paging/v3-overview) - Library that helps load and display pages of data from large dataset, local storage or network.
  - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Helper class for implementing ViewModels that are retained during configuration changes.
- [Coroutines](https://kotlinlang.org/docs/coroutines-overview.html) - Kotlin language functionality that simplifies writing asynchronous/non-blocking code.
- [Flow](https://developer.android.com/kotlin/flow) - Kotlin language functionality that produces a stream of data, asynchronously, without blocking the main thread. Built upon [Coroutines](https://kotlinlang.org/docs/coroutines-overview.html).
- [Retrofit](https://square.github.io/retrofit/) - REST client for Android which makes it easier to consume RESTful web services.
- [Moshi](https://github.com/square/moshi) - JSON library for Android.
- [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) - Dependency injection library built on top of [Dagger](https://dagger.dev) with automatic lifecycle management for core Android classes. 
- [ConstraintLayout](https://developer.android.com/training/constraint-layout/) - Allows creating large and complex layouts with a flat view hierarchy.
- [Glide](https://bumptech.github.io/glide/) - Image loading/caching library.
- [Timber](https://github.com/JakeWharton/timber) - Logger library which provides improvement upon Android's `Log` class

## Notes
- The project contains some minor `TODO` entries throughout.
- Deals List screen is using `Flow` (and not LiveData) to fetch data from the repository for variety's sake and should eventually be consolidated into a single solution. 

## Roadmap
As this project is a work-in-progress, below is a list of features to implement, code to refactor and bugs to fix.
#### Product
- Follow system-controlled day/night mode - Dark Mode.
- Search game/deal functionality.
- Refresh deals feed functionality.
- Quick filter/sort.
- Dedicated filter & sort screen.
- Unlimited, scrollable deals within game details screen (currently hardcoded to 3).
- Support multiple currencies.
- Deals watch-list.
- Deals email notifications.
#### Tech
- Implement Toolbar.
- Add tests.
- Cache stores locally.
#### Bugs/issues
- Duplicate deals on the main feed (caused by different stores).
- Game image on the Game Details screen is low resolution.

### Miscellaneous
- Run the emulator in a separate window:  
  **Preferences** -> **Tools** -> **Emulator** -> disable **Launch in a tool window**  
  <img width="365" alt="image" src="https://user-images.githubusercontent.com/95324430/208821557-525a44fb-9d69-463a-be0a-075d50973e7d.png">
- Gradle dock tasks not showing/updating:  
  **Preferences** -> **Experimental** -> enable **Do not build Gradle task during Gradle sync**
- Running does not apply code/layout changes, Android 11 and above:  
  **Run** -> **Edit Configurations** -> app&apptv -> enable **Always install with package manager**
- Markdown plugin preview pan not working:  
  Double shift -> Type and pick: **Choose Boot Java Runtime for the IDE** -> Pick **JetBrains Runtime with JCEF** -> Restart the IDE  
  <img width="481" alt="image" src="https://user-images.githubusercontent.com/95324430/193950012-53666af2-fe37-4dbd-a779-dfd8d68baef3.png">
- Display internal module dependencies:
  > ./gradlew app:dependencies --configuration AlphaAxisDebugRuntimeClasspath | grep "project :"
- Display updates for external dependencies (libraries):
  > ./gradlew dependencyUpdates
## Contributing
No contributions are accepted at the moment.

## License

```
MIT License

Copyright (c) 2021 nxHL6PocO2qIlidfEP6A

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to 
the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```