package com.getred.cheapshark.presentation.gamedetails

import com.getred.cheapshark.domain.models.Game
import java.util.*

object Fixtures {

    fun gameA(): Game {
        val retailPrice = 59.99F
        val currentLowest = 21.90F

        return Game(
            Game.GameInfo(
                "The Witcher 3: Wild Hunt - Game of the Year Edition",
                "124923",
                "https://cdn.cloudflare.steamstatic.com/steam/subs/124923/capsule_sm_120.jpg"
            ),
            Game.CheapestPriceEver(14.90F, Date()),
            listOf(
                Game.DealBrief(
                    1,
                    "Q8LEGEvjQtpKZ6+KiS6AOTEKmUC/4F6dkc9+wHD5ToM=",
                    currentLowest,
                    retailPrice,
                    currentLowest / retailPrice * 100
                )
            ),
            retailPrice,
            currentLowest
        )
    }
}