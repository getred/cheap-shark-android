package com.getred.cheapshark.presentation.gamedetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.getred.cheapshark.data.Repository
import com.getred.cheapshark.data.network.api.ApiStatus
import com.getred.cheapshark.data.network.api.ApiStatus.*
import com.getred.cheapshark.utils.CoroutinesTestRule
import com.google.common.truth.Truth.assertThat
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class GameDetailsViewModelTest {

    @MockK
    lateinit var repository: Repository

    private lateinit var viewModel: GameDetailsViewModel
    private val gameId = "someGameId"
    private val dealId = "someDealId"

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    // For LiveData, swaps background asynchronous task executor with a synchronous one
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = GameDetailsViewModel(repository)
    }

    @Test
    fun `Given successful response, When fetching game details, Then ApiStatus should be called only with correct values`() {
        // Given
        val observer = createMockObserver()
        viewModel.apiStatus.observeForever(observer)
        coEvery { repository.getGame(gameId) } returns Fixtures.gameA()

        // When
        viewModel.getGameDetails(gameId)

        // Then
        val slots = mutableListOf<ApiStatus>()
        verify { observer.onChanged(capture(slots)) }

        assertThat(slots[0]).isEqualTo(LOADING)
        assertThat(slots[1]).isEqualTo(DONE)
        assertThat(slots.size).isEqualTo(2)
    }

    @Test
    fun `Given successful response, When fetching game details, Then game LiveData should be as fetched data`() {
        // Given
        val observer = createMockObserver()
        val game = Fixtures.gameA()
        viewModel.apiStatus.observeForever(observer)
        coEvery { repository.getGame(gameId) } returns game

        // When
        viewModel.getGameDetails(gameId)

        // Then
        val slots = mutableListOf<ApiStatus>()
        verify { observer.onChanged(capture(slots)) }

        assertThat(viewModel.game.value).isEqualTo(game)
    }

    @Test
    fun `Given error response, When fetching game details, Then ApiStatus should be called only with correct values`() {
        // Given
        val observer = createMockObserver()
        viewModel.apiStatus.observeForever(observer)
        coEvery { repository.getGame(gameId) } coAnswers {
            throw UnknownHostException("Unable to resolve host \"www.cheapshark.com\": No address associated with hostname")
        }

        // When
        viewModel.getGameDetails(gameId)

        // Then
        val slots = mutableListOf<ApiStatus>()
        verify { observer.onChanged(capture(slots)) }

        assertThat(slots[0]).isEqualTo(LOADING)
        assertThat(slots[1]).isEqualTo(ERROR)
        assertThat(slots.size).isEqualTo(2)
    }

    @Test
    fun `Given error response, When fetching game details, Then game LiveData should be null`() {
        // Given
        val observer = createMockObserver()
        viewModel.apiStatus.observeForever(observer)
        coEvery { repository.getGame(gameId) } coAnswers {
            throw UnknownHostException("Unable to resolve host \"www.cheapshark.com\": No address associated with hostname")
        }

        // When
        viewModel.getGameDetails(gameId)

        // Then
        val slots = mutableListOf<ApiStatus>()
        verify { observer.onChanged(capture(slots)) }

        assertThat(viewModel.game.value).isNull()
    }

    @Test
    fun `Given, When navigate to deal event occurs, Then navigateToDeal LiveData should equal correct deal ID`() {
        viewModel.navigateToDeal(dealId)

        assertThat(viewModel.navigateToDeal.value).isEqualTo(dealId)
    }

    @Suppress("ObjectLiteralToLambda")
    private fun createMockObserver(): Observer<ApiStatus> {
        // Kotlin 1.5+ change, see below:
        // https://stackoverflow.com/questions/67736717/mockk-spky-throwing-noclassdeffounderror
        val observer = object : Observer<ApiStatus> {

            override fun onChanged(value: ApiStatus) { }
        }

        return spyk<Observer<ApiStatus>>(observer)
    }
}
