package com.getred.cheapshark.data.network.api

enum class ApiStatus { LOADING, ERROR, DONE }