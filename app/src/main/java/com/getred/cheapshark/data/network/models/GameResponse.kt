package com.getred.cheapshark.data.network.models

import java.util.*

data class GameResponse(
    val info: GameInfo,
    val cheapestPriceEver: CheapestPriceEver,
    val deals: List<DealBrief>

) {
    data class GameInfo(
        val title: String,
        val steamAppID: String?,
        val thumb: String
    )

    data class CheapestPriceEver(
        val price: Float,
        val date: Date
    )

    data class DealBrief(
        val storeID: Int,
        val dealID: String,
        val price: Float,
        val retailPrice: Float,
        val savings: Float,
    )
}