package com.getred.cheapshark.data.network.api

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*
import java.util.concurrent.TimeUnit

@Suppress("unused")
class DateTimeAdapter {

    @ToJson
    fun toJson(date: Date): String {
        return date.time.toString()
    }

    @FromJson
    fun fromJson(value: String): Date {
        // API returned time is in seconds
        return Date(TimeUnit.SECONDS.toMillis(value.toLong()))
    }
}