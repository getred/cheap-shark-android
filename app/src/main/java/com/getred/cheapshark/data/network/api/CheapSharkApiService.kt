package com.getred.cheapshark.data.network.api

import com.getred.cheapshark.BuildConfig
import com.getred.cheapshark.data.network.models.DealResponse
import com.getred.cheapshark.data.network.models.GameResponse
import com.getred.cheapshark.data.network.models.StoreResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface CheapSharkApiService {

    @GET("deals")
    suspend fun getDeals(
        @Query("pageNumber") page: Int? = null,
        @Query("pageSize") size: Int
    ): List<DealResponse>

    @GET("games")
    suspend fun getGame(@Query("id") gameId: String): GameResponse

    @GET("stores")
    suspend fun getStores(): List<StoreResponse>

    companion object {
        fun create(): CheapSharkApiService {
            val logger = HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .add(DateTimeAdapter())
                .build()

            return Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
                .create(CheapSharkApiService::class.java)
        }
    }
}