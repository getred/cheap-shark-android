package com.getred.cheapshark.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.getred.cheapshark.data.Repository.Companion.NETWORK_PAGE_SIZE
import com.getred.cheapshark.data.network.api.CheapSharkApiService
import com.getred.cheapshark.data.network.models.DealResponse

private const val CHEAP_SHARK_INITIAL_PAGE_INDEX = 0

class DealsPagingSource(private val apiService: CheapSharkApiService) :
    PagingSource<Int, DealResponse>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, DealResponse> {
        val page = params.key ?: CHEAP_SHARK_INITIAL_PAGE_INDEX

        return try {
            val deals = apiService.getDeals(page, params.loadSize)
            val nextKey = if (deals.isEmpty()) {
                null
            } else {
                // First load fetches 3 pages worth of items, so next page should be 3 (0-indexed)
                page + (params.loadSize / NETWORK_PAGE_SIZE)
            }

            LoadResult.Page(
                data = deals,
                prevKey = if (page == CHEAP_SHARK_INITIAL_PAGE_INDEX) null else page - 1,
                nextKey = nextKey
            )
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, DealResponse>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}