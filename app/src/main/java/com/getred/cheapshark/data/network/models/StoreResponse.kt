package com.getred.cheapshark.data.network.models

data class StoreResponse(
    val storeID: Int,
    val storeName: String,
    val isActive: Int,
    val images: StoreLogos
) {
    data class StoreLogos(
        val banner: String,
        val logo: String,
        val icon: String
    )
}
