package com.getred.cheapshark.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.getred.cheapshark.data.network.api.CheapSharkApiService
import com.getred.cheapshark.domain.mappers.toDomainModel
import com.getred.cheapshark.domain.models.Deal
import com.getred.cheapshark.domain.models.Game
import com.getred.cheapshark.domain.models.Store
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiService: CheapSharkApiService) {
    private var stores: List<Store>? = null

    private suspend fun getStores(): List<Store> {
        return stores ?: apiService.getStores().toDomainModel().also {
            stores = it
        }
    }

    fun getDealsStream(): Flow<PagingData<Deal>> {
        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
            pagingSourceFactory = { DealsPagingSource(apiService) }
        ).flow.map { pagingData ->
            pagingData.map { dealResponse ->
                dealResponse.toDomainModel()
            }
        }
    }

    suspend fun getGame(gameId: String): Game {
        val game = apiService.getGame(gameId).toDomainModel()

        // Add Store model into each Game.DealBrief
        game.deals.forEach { dealBrief ->
            getStores().find { store -> store.storeId == dealBrief.storeId }?.let { foundStore ->
                dealBrief.store = foundStore
            }

        }

        return game
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 10
    }
}