package com.getred.cheapshark.data.network.models

import java.util.*

data class DealResponse(
    val internalName: String,
    val title: String,
    val dealID: String,
    val storeID: String,
    val gameID: String,
    val salePrice: Float,
    val normalPrice: Float,
    val savings: Float,
    val releaseDate: Date,
    val lastChange: Date,
    val dealRating: Float,
    val thumb: String
)