package com.getred.cheapshark.data.network

import com.getred.cheapshark.data.network.api.CheapSharkApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideCheapSharkApiService(): CheapSharkApiService {
        return CheapSharkApiService.create()
    }
}