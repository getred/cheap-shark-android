package com.getred.cheapshark.presentation.gamedetails

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.getred.cheapshark.databinding.FragmentGameDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameDetailsFragment : Fragment() {
    private val viewModel: GameDetailsViewModel by viewModels()
    private val arguments: GameDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentGameDetailsBinding.inflate(inflater)

        viewModel.navigateToDeal.observe(viewLifecycleOwner, { dealId ->
            dealId?.let { navigateToDeal(dealId) }
        })

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getGameDetails(arguments.gameId)
    }

    private fun navigateToDeal(dealUrl: String) {
        startActivity(Intent(ACTION_VIEW).apply {
            this.data = dealUrl.toUri()
        })
    }
}