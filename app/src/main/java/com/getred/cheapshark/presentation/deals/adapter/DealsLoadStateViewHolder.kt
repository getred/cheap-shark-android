package com.getred.cheapshark.presentation.deals.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.getred.cheapshark.R
import com.getred.cheapshark.databinding.DealItemLoadStateFooterBinding

class DealsLoadStateViewHolder(private val binding: DealItemLoadStateFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(loadState: LoadState, retry: () -> Unit) {
        if (loadState is LoadState.Error) {
            binding.errorMessage.text = loadState.error.localizedMessage
        }

        binding.progressBar.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState is LoadState.Error
        binding.errorMessage.isVisible = loadState is LoadState.Error
        binding.retryButton.setOnClickListener { retry.invoke() }
    }

    companion object {
        fun create(parent: ViewGroup): DealsLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.deal_item_load_state_footer, parent, false)
            val binding = DealItemLoadStateFooterBinding.bind(view)

            return DealsLoadStateViewHolder(binding)
        }
    }
}