package com.getred.cheapshark.presentation.gamedetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.getred.cheapshark.data.Repository
import com.getred.cheapshark.data.network.api.ApiStatus
import com.getred.cheapshark.data.network.api.ApiStatus.*
import com.getred.cheapshark.domain.models.Game
import com.getred.cheapshark.presentation.utilities.SingleLiveDataEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class GameDetailsViewModel @Inject constructor(
    private val repository: Repository
) :
    ViewModel() {
    private val _apiStatus = MutableLiveData<ApiStatus>()
    private val _game = MutableLiveData<Game>()
    private val _navigateToDeal = SingleLiveDataEvent<String>()

    val apiStatus: LiveData<ApiStatus>
        get() = _apiStatus

    val game: LiveData<Game>
        get() = _game

    val navigateToDeal: LiveData<String>
        get() = _navigateToDeal

    fun getGameDetails(gameId: String) {
        viewModelScope.launch {
            _apiStatus.value = LOADING

            try {
                _game.value = repository.getGame(gameId)
                _apiStatus.value = DONE
            } catch (exception: Exception) {
                Timber.e(exception)
                _apiStatus.value = ERROR
                _game.value = null
            }
        }
    }

    fun navigateToDeal(dealId: String) {
        _navigateToDeal.value = dealId
    }
}