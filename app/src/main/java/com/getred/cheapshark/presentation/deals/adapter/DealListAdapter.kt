package com.getred.cheapshark.presentation.deals.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.getred.cheapshark.databinding.DealItemBinding
import com.getred.cheapshark.domain.models.Deal
import com.getred.cheapshark.presentation.deals.DealsViewModel

class DealListAdapter(private val viewModel: DealsViewModel) :
    PagingDataAdapter<Deal, DealListAdapter.DealViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return DealViewHolder(DealItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: DealViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(viewModel, it)
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Deal>() {

        override fun areItemsTheSame(oldItem: Deal, newItem: Deal): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Deal, newItem: Deal): Boolean {
            return oldItem.dealId == newItem.dealId
        }

    }

    class DealViewHolder(private val binding: DealItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: DealsViewModel, deal: Deal) {
            binding.deal = deal
            binding.viewModel = viewModel
            binding.executePendingBindings()
        }
    }
}