package com.getred.cheapshark.presentation.deals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.getred.cheapshark.data.Repository
import com.getred.cheapshark.domain.models.Deal
import com.getred.cheapshark.presentation.utilities.SingleLiveDataEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class DealsViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val _deals = MutableLiveData<List<Deal>>()
    private val _navigateToGameDetails = SingleLiveDataEvent<String>()
    private val _retryButtonClicked = SingleLiveDataEvent<Boolean>()

    private var currentDealsResult: Flow<PagingData<Deal>>? = null

    val deals: LiveData<List<Deal>>
        get() = _deals

    val navigateToGameDetails: LiveData<String>
        get() = _navigateToGameDetails

    val retryButtonClicked: LiveData<Boolean>
        get() = _retryButtonClicked

    fun getDealList(): Flow<PagingData<Deal>> {
        val lastResults = currentDealsResult
        // Simple implementation to prevent re-fetching on configuration change
        if (lastResults != null) {
            return lastResults
        }

        val newResults = repository.getDealsStream().cachedIn(viewModelScope)
        currentDealsResult = newResults

        return newResults
    }

    fun navigateToGameDetails(gameId: String) {
        _navigateToGameDetails.value = gameId
    }

    fun retryButtonClicked() {
        _retryButtonClicked.value = true
    }
}