package com.getred.cheapshark.presentation.deals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.getred.cheapshark.R
import com.getred.cheapshark.databinding.FragmentDealsBinding
import com.getred.cheapshark.presentation.deals.adapter.DealListAdapter
import com.getred.cheapshark.presentation.deals.adapter.DealsLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DealsFragment : Fragment() {
    private val viewModel: DealsViewModel by viewModels()
    private var dealsJob: Job? = null
    private lateinit var adapter: DealListAdapter
    private lateinit var binding: FragmentDealsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDealsBinding.inflate(inflater)

        setItemDecoration(binding)

        viewModel.navigateToGameDetails.observe(viewLifecycleOwner, { gameId ->
            gameId?.let { navigateToGameDetails(gameId) }
        })

        viewModel.retryButtonClicked.observe(viewLifecycleOwner, {
            it?.let {
                adapter.retry()
            }
        })

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        initAdapter()

        getDealList()

        return binding.root
    }

    private fun initAdapter() {
        adapter = DealListAdapter(viewModel)
        binding.dealsList.adapter = adapter.withLoadStateHeaderAndFooter(
            header = DealsLoadStateAdapter { adapter.retry() },
            footer = DealsLoadStateAdapter { adapter.retry() }
        )

        adapter.addLoadStateListener { loadState ->
            when (loadState.refresh) {
                is LoadState.Loading -> setLoadingState()
                is LoadState.NotLoading -> setDoneLoadingState(adapter.itemCount == 0)
                is LoadState.Error -> setErrorState(loadState)
            }
        }
    }

    private fun setLoadingState() {
        binding.progressBar.isVisible = true
        binding.retryButton.isVisible = false
        binding.initialResponseTextView.isVisible = false
        binding.dealsList.isVisible = false
    }

    private fun setErrorState(loadState: CombinedLoadStates) {
        binding.progressBar.isVisible = false
        binding.retryButton.isVisible = true
        binding.initialResponseTextView.isVisible = true

        val errorState = loadState.refresh as LoadState.Error
        binding.initialResponseTextView.text = errorState.error.localizedMessage
        binding.dealsList.isVisible = false
    }

    private fun setDoneLoadingState(noResults: Boolean) {
        binding.progressBar.isVisible = false
        binding.retryButton.isVisible = false

        if (noResults) {
            binding.initialResponseTextView.isVisible = true
            binding.initialResponseTextView.text = getString(R.string.no_results)
            binding.dealsList.isVisible = false
        } else {
            binding.initialResponseTextView.isVisible = false
            binding.dealsList.isVisible = true
        }
    }

    private fun getDealList() {
        dealsJob?.cancel()

        dealsJob = lifecycleScope.launch {
            viewModel.getDealList().collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun navigateToGameDetails(gameId: String) {
        this.findNavController()
            .navigate(
                DealsFragmentDirections.actionNavigateToGameDetails(gameId)
            )
    }

    private fun setItemDecoration(binding: FragmentDealsBinding) {
        val divider = DividerItemDecoration(
            binding.root.context,
            (binding.dealsList.layoutManager as LinearLayoutManager).orientation
        )

        binding.dealsList.addItemDecoration(divider)
    }
}