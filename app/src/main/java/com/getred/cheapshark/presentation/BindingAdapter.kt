package com.getred.cheapshark.presentation

import android.annotation.SuppressLint
import android.text.format.DateFormat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.getred.cheapshark.R
import com.getred.cheapshark.data.network.api.ApiStatus
import com.getred.cheapshark.data.network.api.ApiStatus.*
import java.util.*

@BindingAdapter("apiStatus")
fun ImageView.bindApiStatus(status: ApiStatus) {
    when (status) {
        LOADING -> {
            this.visibility = View.VISIBLE
            this.setImageResource(R.drawable.loading_animation)
        }

        ERROR -> {
            this.visibility = View.VISIBLE
            this.setImageResource(R.drawable.ic_connection_error)
        }

        DONE -> this.visibility = View.GONE
    }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("savingsPercentage")
fun TextView.savingsPercentage(savings: Float) {
    this.text = "-${savings.toInt()}%"
}

// Refactor after introducing currencies
@SuppressLint("StringFormatMatches")
@BindingAdapter("priceValue")
fun TextView.priceValue(dealPrice: Float) {
    this.text = when (dealPrice) {
        0f -> this.resources.getString(R.string.savings_free_label)
        else -> "$dealPrice$"
    }
}

@BindingAdapter("dateValue")
fun TextView.dateValue(date: Date?) {
    date?.let {
        this.text = DateFormat.getMediumDateFormat(this.context).format(date)
    }
}

@BindingAdapter("imageUrl")
fun ImageView.bindImage(imageUrl: String?) {
    imageUrl?.let {
        val iconCornerRadius =
            this.context.resources.getDimensionPixelSize(R.dimen.game_icon_corner_radius)

        Glide.with(this.context)
            .load(it)
            .transform(CenterCrop(), RoundedCorners(iconCornerRadius))
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(this)
    }
}