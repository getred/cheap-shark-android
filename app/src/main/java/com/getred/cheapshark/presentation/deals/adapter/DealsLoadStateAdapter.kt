package com.getred.cheapshark.presentation.deals.adapter

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

// For some reason, using android:onClick data binding from within the LoadStateAdapter does NOT work
// despite the regular adapter working as expected, further investigation is required.
// Passing the retry lambda as an alternative solution
class DealsLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<DealsLoadStateViewHolder>() {

    override fun onBindViewHolder(holder: DealsLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState, retry)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): DealsLoadStateViewHolder = DealsLoadStateViewHolder.create(parent)
}