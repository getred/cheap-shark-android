package com.getred.cheapshark.domain.mappers

import com.getred.cheapshark.data.network.models.DealResponse
import com.getred.cheapshark.domain.models.Deal

fun DealResponse.toDomainModel() = Deal(
    internalName = this.internalName,
    title = this.title,
    dealId = this.dealID,
    storeId = this.storeID,
    gameId = this.gameID,
    dealPrice = this.salePrice,
    retailPrice = this.normalPrice,
    savings = this.savings,
    releaseDate = this.releaseDate,
    lastChange = this.lastChange,
    dealRating = this.dealRating,
    thumbnail = this.thumb
)