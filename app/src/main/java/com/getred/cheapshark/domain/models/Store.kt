package com.getred.cheapshark.domain.models

data class Store(
    val storeId: Int,
    val storeName: String,
    val isActive: Boolean,
    val storeLogos: StoreLogos
) {

    data class StoreLogos(
        val bannerUrl: String,
        val logoUrl: String,
        val iconUrl: String
    )
}