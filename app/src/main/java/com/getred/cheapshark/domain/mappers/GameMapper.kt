package com.getred.cheapshark.domain.mappers

import com.getred.cheapshark.BuildConfig
import com.getred.cheapshark.data.network.models.GameResponse
import com.getred.cheapshark.domain.models.Game

fun GameResponse.toDomainModel() = Game(
    info = this.info.toDomainModel(),
    cheapestPriceEver = this.cheapestPriceEver.toDomainModel(),
    deals = this.deals.toDomainModel(),
    retailPrice = deals[0].retailPrice,
    currentLowest = deals.minByOrNull { dealBrief -> dealBrief.price }?.price
        ?: 999F // Null should never happen
)

fun GameResponse.GameInfo.toDomainModel() = Game.GameInfo(
    title = this.title,
    steamAppId = this.steamAppID,
    thumbnail = this.thumb
)

fun GameResponse.CheapestPriceEver.toDomainModel() = Game.CheapestPriceEver(
    price = this.price,
    date = this.date
)

fun GameResponse.DealBrief.toDomainModel() = Game.DealBrief(
    storeId = this.storeID,
    dealId = this.dealID,
    price = this.price,
    retailPrice = this.retailPrice,
    savings = this.savings,
).apply {
    dealUrl = BuildConfig.DEAL_BASE_URL + dealId
}

fun List<GameResponse.DealBrief>.toDomainModel(): List<Game.DealBrief> {
    return this.map { it.toDomainModel() }
}