package com.getred.cheapshark.domain.models

import java.util.*

data class Game(
    val info: GameInfo,
    val cheapestPriceEver: CheapestPriceEver,
    val deals: List<DealBrief>,
    val retailPrice: Float,
    val currentLowest: Float
) {

    data class GameInfo(
        val title: String,
        val steamAppId: String?,
        val thumbnail: String
    )

    data class CheapestPriceEver(
        val price: Float,
        val date: Date
    )

    data class DealBrief(
        val storeId: Int,
        val dealId: String,
        val price: Float,
        val retailPrice: Float,
        val savings: Float,
    ) {
        lateinit var store: Store
        lateinit var dealUrl: String
    }
}
