package com.getred.cheapshark.domain.mappers

import com.getred.cheapshark.BuildConfig
import com.getred.cheapshark.data.network.models.StoreResponse
import com.getred.cheapshark.domain.models.Store

fun StoreResponse.toDomainModel() = Store(
    storeId = this.storeID,
    storeName = this.storeName,
    isActive = this.isActive == 1,
    storeLogos = this.images.toDomainModel()
)

fun StoreResponse.StoreLogos.toDomainModel() = Store.StoreLogos(
    bannerUrl = BuildConfig.WEB_BASE_URL + this.banner,
    logoUrl = BuildConfig.WEB_BASE_URL + this.logo,
    iconUrl = BuildConfig.WEB_BASE_URL + this.icon
)

fun List<StoreResponse>.toDomainModel() = this.map { it.toDomainModel() }