package com.getred.cheapshark.domain.models

import java.util.*

data class Deal(
    val internalName: String,
    val title: String,
    val dealId: String,
    val storeId: String,
    val gameId: String,
    val dealPrice: Float,
    val retailPrice: Float,
    val savings: Float,
    val releaseDate: Date,
    val lastChange: Date,
    val dealRating: Float,
    val thumbnail: String
)